<div align="center">

 ***
Данный репозиторий используется для мульти-язяковой системы с проекта **FLUXMINE (mc.fluxmine.fun)**
</div>

---
## Обратная связь
Если у Вас есть какие-то предложения или желание в помощи по переводу, 
то можете обратиться к нам через указанные контакты:

* **[Discord](https://dsc.gg/fluxmine)**
